===========
Kosz Python
===========

A Basket and how for all orphan Python

A Basket of orphan $language code that might be useful for
anyone anywhere anytime to solve larger adventures. But to
small or not ready for their own packages.

* Free software: MIT license

* **BE WARN package is:**

1. not FOUND on any "$language Package Index".
2. Contain code not tested and can be removed.
3. Might start out messy
3. SimVer might be a bit chaotic

Table of Contents
---------------------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
